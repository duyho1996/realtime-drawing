var express      = require('express');
var app          = require('express')();
var server       = require('http').Server(app);
var io           = require('socket.io')(server);
const bodyParser = require('body-parser');

function normalizePort(val) {
	var port = parseInt(val, 10);

	if (isNaN(port)) {
		// named pipe
		return val;
	}

	if (port >= 0) {
		// port number
		return port;
	}

	return false;
}
const port = normalizePort(process.env.PORT || '80');
server.listen(port);

app.set('port', port);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use('/assets', express.static('assets'));

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');
});

app.post('/broadcast-event', function (req, res) {
	io.emit('message', req.body);
	res.end("");
});

io.on('error', function (socket) {
	console.log('error')
});

io.on('connection', function (socket) {
	console.log('connected');
	socket.on('broadcast-event', function (data) {
		io.emit('message', data);
	});
});
