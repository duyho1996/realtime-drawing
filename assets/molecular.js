var Molecular = function (bottomRightBoundedPoint) {
	this.direction = {x: 1, y: 1};
	this.position  = {x: 0, y: 0};

	this.bottomRightBoundedPoint = bottomRightBoundedPoint;
	this.init();
};

Molecular.prototype.bottomRightBoundedPoint = {x: 300, y: 300};

Molecular.prototype.move         = function () {
	this.position.x += this.direction.x;
	this.position.y += this.direction.y;
	if (this.position.x < 0 || this.position.x > this.bottomRightBoundedPoint.x) {
		this.direction.x *= -1;
	}
	else if (this.position.y < 0 || this.position.y > this.bottomRightBoundedPoint.y) {
		this.direction.y *= -1;
	}
};
Molecular.prototype.init         = function () {
	this.setPosition();
	this.setDirection();
};
Molecular.prototype.setPosition  = function () {
	var x           = this.getRandom(0, this.bottomRightBoundedPoint.x);
	var y           = this.getRandom(0, this.bottomRightBoundedPoint.y);
	this.position.x = x;
	this.position.y = y;
};
Molecular.prototype.setDirection = function () {
	var x = this.getRandom(-1, 1, true);
	var y = this.getRandom(-1, 1, true);
	while (x === 0 && y === 0) {
		y = this.getRandom(-1, 1, true);
	}
	this.direction.x = x;
	this.direction.y = y;
};

Molecular.prototype.getRandom = function (min, max, isInteger = false) {
	if (isInteger) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	return Math.random() * (max - min) + min;
};

Molecular.prototype.draw = function (context) {
	context.beginPath();
	context.arc(this.position.x, this.position.y, 2, 0, 2 * Math.PI);
	context.strokeStyle = "#FFF";
	context.lineWidth   = 1.2;
	context.stroke();
};


function MolecularBoard(canvas) {
	this.canvas = canvas;
	this.context = canvas.getContext('2d');
	this.width = canvas.width;
	this.height = canvas.height;
}

MolecularBoard.prototype.looper     = null;
MolecularBoard.prototype.moleculars = [];
MolecularBoard.prototype.init       = function (molecularNum = 80) {
	this.moleculars = [];
	for (var i = 0; i < molecularNum; i++) {
		var item = new Molecular({x: this.width, y: this.height});
		this.moleculars.push(item);
	}
};
MolecularBoard.prototype.run        = function () {
	this.looper = setInterval(() => {
		this.context.clearRect(0, 0, this.width, this.height);
		for (var index in this.moleculars) {
			var molecular = this.moleculars[index];
			molecular.move();
			molecular.draw(this.context);
		}
		this.drawConnection()
	}, 30);
};

MolecularBoard.prototype.drawConnection = function () {
	for (var i = 0; i < this.moleculars.length; i++) {
		var checkingMolecular = this.moleculars[i];
		for (var j = i + 1; j < this.moleculars.length; j++) {
			var candidateMolecular = this.moleculars[j];
			var distance           = this.getDistance(checkingMolecular.position, candidateMolecular.position);
			if (distance < 70) {
				this.context.beginPath();
				this.context.moveTo(checkingMolecular.position.x, checkingMolecular.position.y);
				this.context.lineTo(candidateMolecular.position.x, candidateMolecular.position.y);
				this.context.strokeStyle = "#FFF";
				this.context.lineWidth   = 1.2 - (distance / 60);
				this.context.stroke();
			}
		}
	}
};

MolecularBoard.prototype.getDistance = function (position1, position2) {
	return Math.sqrt((position1.x - position2.x) * (position1.x - position2.x) + (position1.y - position2.y) * (position1.y - position2.y))
};

MolecularBoard.prototype.add = function (x, y) {
	var item = new Molecular({x: this.width, y: this.height});
	item.position.x = x;
	item.position.y = y;
	this.moleculars.push(item);
};